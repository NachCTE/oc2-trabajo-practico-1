#include <stdio.h>
#include <stdlib.h>
extern void formulaResolvente(float A, float B, float C);

int main(){
    float A,B,C;

    printf("Formula resolvente automatica\n\n");

    printf("Ingrese A (X^2):  ");
    scanf("%f", &A );

    printf("Ingrese B (X):  ");
    scanf("%f", &B );

    printf("Ingrese C (Termino independiente):  ");
    scanf("%f", &C );

    printf("Formula ingresada: (%.2f X^2)+(%.2f X)+(%.2f)=0\n", A, B, C);
    
    formulaResolvente(A,B,C);

    return 0;
}