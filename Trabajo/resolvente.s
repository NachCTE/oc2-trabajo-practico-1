;Autor: Conte Ignacio Nicolas

global formulaResolvente
extern printf

section .data
dos dq 2.0
cuatro dq 4.0
valorA dq 0.0
valorB dq 0.0
valorC dq 0.0
fmtUno db "R1: %f", 10, 0
fmtDos db "R2: %f", 10, 0

section .bss
dosA resq 1
menosB resq 1
Bcuadrado resq 1
CuatroAC resq 1
resta resq 1
raiz resq 1
menosBmasRaiz resq 1
menosBmenosRaiz resq 1
resultadoBMas resq 1
resultadoBMenos resq 1

section .text
formulaResolvente:
    ;Preparo la pila
    push ebp
    mov ebp,esp
    FINIT
    fld dword [ebp+8]
    fst qword [valorA]
    FINIT
    fld dword [ebp+12]
    fst qword [valorB]
    FINIT
    fld dword [ebp+16]
    fst qword [valorC]

    ;Obtengo dos * A
    FINIT
    fld qword [dos]
    fld qword [valorA]
    fmul
    fst qword [dosA]
    
    ;Obtengo -B
    FINIT
    fld qword [valorB]
    fchs
    fst qword [menosB]
    
    ;Obtengo B^2
    FINIT
    fld qword [valorB]
    fmul st0, st0
    fst qword [Bcuadrado]
   
    ;Obtengo 4*A*C
    FINIT
    fld qword [cuatro]
    fld qword [valorA]
    fmul 
    fld qword [valorC]
    fmul 
    fst qword [CuatroAC]
    
    ;Obtengo B^2 - 4*A*C
    FINIT
    fld qword [CuatroAC]
    fld qword [Bcuadrado]
    fsub st0, st1
    fst qword [resta]
    
    ;Obtengo la raiz
    FINIT
    fld qword [resta]
    fsqrt
    fst qword [raiz]
    
    ;Obtengo -B + Raiz
    FINIT
    fld qword [raiz]
    fld qword [menosB]
    fadd st0, st1
    fst qword [menosBmasRaiz]
    
    ;Obtengo -B - Raiz
    FINIT
    fld qword [raiz]
    fld qword [menosB]
    fsub st0, st1
    fst qword [menosBmenosRaiz]
    
    ;Obtengo (-B + Raiz)/2
    FINIT
    fld qword [dosA]
    fld qword [menosBmasRaiz]
    fdiv st0, st1
    fst qword [resultadoBMas]
    
    ;Obtengo (-B - Raiz)/2
    FINIT
    fld qword [dosA]
    fld qword [menosBmenosRaiz]
    fdiv st0, st1
    fst qword [resultadoBMenos]
    
    ;Imprimo el resultado 1
    push dword [resultadoBMas+4]
    push dword [resultadoBMas]
    push fmtUno
    call printf
    add ESP, 12
    
    ;Imprimo el resultado 2
    push dword [resultadoBMenos+4]
    push dword [resultadoBMenos]
    push fmtDos
    call printf
    add ESP, 12

    ;Devuelvo la pila al estado original
    mov ebp, esp
    pop ebp
    
    ret