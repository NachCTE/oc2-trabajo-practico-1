global main
extern printf

section .data
vectorFloat dd 50.5,5.25,3.60,72.20,50.0
lenghtVector dd 4               ;lenght del vector -1 (cantidad de veces que debe ejecutarse el ciclo)
fmt db "Resultado: %f", 10,13,0

section .bss
suma resq 1

section .text

main:
    mov ebp, esp; for correct debugging
    push vectorFloat
    call ASMsuma
    add esp, 4

imprimir:                       ;imprimo la variable "suma"
    push dword[suma+4]          
    push dword[suma] 
    push fmt
    call printf
    add esp, 12
    ret
    
ASMsuma:
    push ebp                    ;preparo el stack
    mov ebp, esp        
    mov eax , [EBP+8]           ;ebp+8 es la posicion del stack del puntero al vector
    fld dword[eax]              ;agrego el primer valor del vector a la pila de fpu
    mov ebx, 1                  ;ebx indicara en que posicion del vector estamos
    mov ecx, [lenghtVector]     ;cantidad de veces a realizarse el ciclo
    
ciclo:
    fld dword[eax +4*ebx]       ;agrego a la pila de la fpu el siguiente valor del vector
    fadd                        ;sumo las posiciones 0 y 1 de la pila 
    inc ebx                     ;incremento ebx para indicar que debemos tomar el proximo valor
    loop ciclo

    fst qword[suma]             ;al salir del ciclo agrego la suma total a la variable "suma"
    mov esp, ebp
    pop ebp
    ret