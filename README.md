# Indice
- [Indice](#indice)
- [Introduccion](#introduccion)
- [Ejecucion](#ejecucion)
- [Funcionamiento](#funcionamiento)
  * [Assembler](#assembler)
  * [C](#c)

# Introduccion

En este repositorio de GIT se puede encontrar el trabajo practico 1 de la pateria Organizacion del Computador 2, de la carrera de sistemas de la UNGS.

Este trabajo practico consta de 2 archivos: un archivo assembler IA-32 (.s) y un archivo C.

La funcionalidad que este sistema busca otorgar es dar los resultados de una funcion cuadratica, luego de que el usuario ingrese los valores A, B y C de la misma.

**Si la funcion cuadratica no tiene resultados (El interior de la raiz da un numero negativo o 2*A = 0, entonces el programa mostrara los resultados como nulos)**

Ademas del programa principal, tambien se incluyen los ejercicios 4, 6 y 7 de la practica de gestion de memoria, y el ejercicio 4 de la practica de FPU.

# Ejecucion
Para ejecutar el programa, dirijase al directorio **/oc2-trabajo-practico-1/Trabajo**, y en la terminal ejecute el siguiente comando:

```bash
bash ejecutar.sh
```

El archivo .sh se encargará de compilar el codigo asm y el codigo c, linkearlos y ejecutar el programa.

# Funcionamiento 

En este apartado se explicara como fue resuelto el problema y que metodos fueron utilizados, y se mostraran capturas del funcionamiento del programa.

**En este caso, fueron utilizados los valores A=4.50 B=10.20, C=5.50**

## Assembler

Desde el lado del assembler, se busco resolver la ecuacion separandola en terminos, realizando operaciones intermedias entre los valores A, B y C y guardando los resultados de las mismas en variables definidas en memoria, para luego obtener con estos los resultados finales.

A continuacion seran explicados los distintos fragmentos de codigo que contiene el archivo assembler.

----
### Inicializacion de variables

```nasm
section .data
dos dq 2.0
cuatro dq 4.0
valorA dq 0.0
valorB dq 0.0
valorC dq 0.0
fmtUno db "R1: %f", 10, 0
fmtDos db "R2: %f", 10, 0
```
Se inicializan los valores que utilizaremos luego (como 2 y 4), los formatos en los cuales se imprimiran los resultados, y las variables A,B y C en 0 para que no se presente ningun tipo de error al desapilar las mismas (ya que son apiladas desde el archivo C)=. 

----
### Definicion de variables vacias

```nasm
section .bss
dosA resq 1
menosB resq 1
Bcuadrado resq 1
CuatroAC resq 1
resta resq 1
raiz resq 1
menosBmasRaiz resq 1
menosBmenosRaiz resq 1
resultadoBMas resq 1
resultadoBMenos resq 1
```
Se reserva el espacio en memoria para las variables en las cuales se iran almacenando los resultados intermedios de la funcion, y los resultados finales de la misma.

----
### Desapilar parametros

```nasm
section .text
formulaResolvente:
    push ebp
    mov ebp,esp
    FINIT
    fld dword [ebp+8]
    fst qword [valorA]
    FINIT
    fld dword [ebp+12]
    fst qword [valorB]
    FINIT
    fld dword [ebp+16]
    fst qword [valorC]
```
Se prepara la pila para el llamado a una subrutina, apilando ebp e indicando el tope de la pila como nueva base de esta.

FINIT inicializa la FPU, limpiando sus registros para que no haya ningun problema.

Se copian los valores apilados desde C (A,B y C) en los registros de la FPU (ya que son punto flotante, lo cual no permite que se utilicen registros de uso general) y luego se guardan en las variables valorA, valorB y valorC, ya inicializadas

----
### Intermedio: 2 * A
```nasm
    FINIT
    fld qword [dos]
    fld qword [valorA]
    fmul
    fst qword [dosA]
```
En esta instancia comienzan los calculos matematicos sobre las variables A, B y C.

En este caso, limpio la pila de la FPU e ingreso los valores **A = 4.50** y 2, se multiplican y el resultado se guarda en la variable dosA

<br>

_Resultado de la operacion 4.50 * 2_ 

![res1](./ImagenesReadme/resultado1.PNG) 

----
### Intermedio: -B
```nasm
    FINIT
    fld qword [valorB]
    fchs
    fst qword [menosB]
```
Limpio la pila de la FPU e ingreso el valor **B = 10.20**, invierto su signo mediante la funcion **fchs**, y guardo el resultado en la variable menosB

<br>

_Resultado de la operacion de cambio de signo sobre 10.20_ 

![res2](./ImagenesReadme/resultado2.PNG) 

----
### Intermedio: B^2
```nasm
    FINIT
    fld qword [valorB]
    fmul st0, st0
    fst qword [Bcuadrado]
```
Limpio la pila de la FPU e ingreso el valor **B = 10.20**, y lo multiplico por si mismo (st0 * st0, ya que este se encuentra en esa posicion de la pila de FPU) para obtener su cuadrado.

Guardo el resultado en la variable Bcuadrado.

<br>

_Resultado de la operacion 10.20 * 10.20_ 

![res3](./ImagenesReadme/resultado3.PNG) 

----
### Intermedio: 4 * A * C 
```nasm
    FINIT
    fld qword [cuatro]
    fld qword [valorA]
    fmul 
    fld qword [valorC]
    fmul 
    fst qword [CuatroAC]
```
Limpio la pila de la FPU e ingreso los valores 4 y **A = 4.50**, y los multiplico. El resultado se almacena en la pila, por lo cual ingreso el valor **C = 5.50** a la FPU y mutiplico el resutlado anterior por C.

Guardo el resultado en la variable CuatroAC.

<br>

_Resultado de la operacion 4 * 4.50 * 5.50_ 

![res4](./ImagenesReadme/resultado4.PNG) 

----
### Intermedio: (B ^ 2) - (4 * A * C)
```nasm
    FINIT
    fld qword [CuatroAC]
    fld qword [Bcuadrado]
    fsub st0, st1
    fst qword [resta]
```
Limpio la pila de la FPU e ingreso el valor **Bcuadrado = 104.04**, al cual procedo a restarle **CuatroAC = 99** (tambien cargado en la pila de la FPU).

Guardo el resultado en la variable resta.

<br>

_Resultado de la operacion 104.04 - 99_ 

![res5](./ImagenesReadme/resultado5.PNG) 

----
### Intermedio: raiz cuadrada
```nasm
    FINIT
    fld qword [resta]
    fsqrt
    fst qword [raiz]
```
Limpio la pila de la FPU e ingreso el valor el resultado de la resta anterior **(resta = 5.04)**, y calculo la raiz cuadrada de este numero mediante la instruccion **fsqrt**.

Guardo el resultado en la variable raiz.

<br>

_Resultado de la operacion raiz cuadrada de 5.04_ 

![res6](./ImagenesReadme/resultado6.PNG) 

----
### Intermedio: -B + Raiz

En este punto diverge la funcion, y se separa en 2 caminos distintos. En este caso, a -B se le suma la raiz.
```nasm
    FINIT
    fld qword [raiz]
    fld qword [menosB]
    fadd st0, st1
    fst qword [menosBmasRaiz]
```
Limpio la pila de la FPU e ingreso los valores **menosB = -10.20** y **raiz = 2.244994**. Procedo a sumar estos dos valores.

Guardo el resultado en la variable menosBmasRaiz.


<br>

_Resultado de la operacion -B + Raiz_ 

![res7](./ImagenesReadme/resultado7.PNG) 

----
### Intermedio: -B - Raiz

En este punto diverge la funcion, y se separa en 2 caminos distintos. En este caso, a -B se le resta la raiz.
```nasm
    FINIT
    fld qword [raiz]
    fld qword [menosB]
    fdub st0, st1
    fst qword [menosBmenosRaiz]
```
Limpio la pila de la FPU e ingreso los valores **menosB = -10.20** y **raiz = 2.244994**. Procedo a restarle esta cantidad al primer valor.

Guardo el resultado en la variable menosBmenosRaiz.


<br>

_Resultado de la operacion -B - Raiz_ 

![res8](./ImagenesReadme/resultado8.PNG) 

----
### Resultados finales

Este es el ultimo paso de la formula resolvente, en el cual se toman los valores obtenidos en los 2 pasos anteriores (-B + raiz y -B - raiz), y ambos son divididos por 2*A.
```nasm
    FINIT
    fld qword [dosA]
    fld qword [menosBmasRaiz]
    fdiv st0, st1
    fst qword [resultadoBMas]
    
    FINIT
    fld qword [dosA]
    fld qword [menosBmenosRaiz]
    fdiv st0, st1
    fst qword [resultadoBMenos]
```
Limpio la pila de la FPU, ingreso el valor **menosBmasRaiz = -7.955006**, y lo divido por **dosA = 9**. El resultado es almacenado en resutladoBMas.

El mismo procedimiento es realizado para el valor **menosBmenosRaiz = -12.444994**, y su resultado se almacena en resultadoBMenos.

Estos resultados son impresos en pantalla mediante el siguiente fragmento de codigo:
```nasm
    push dword [resultadoBMas+4]
    push dword [resultadoBMas]
    push fmtUno
    call printf
    add ESP, 12
    
    ;Imprimo el resultado 2
    push dword [resultadoBMenos+4]
    push dword [resultadoBMenos]
    push fmtDos
    call printf
    add ESP, 12 
```
Mostrando la siguiente salida:

![resFinal](./ImagenesReadme/resultadoFinal.PNG)

----

## C
Por el lado de C, se busca que el usuario pueda realizar un input de los valores que quiera utilizar en la formula resolvente.

Para poder llevar a cabo la ejecucion completa del programa, el archivo C declara a la funcion **formulaResolvente** y realiza un llamado a la misma, pasandole los valores que el usuario ingreso en los input.

Para que el archivo C encuentre la funcion resolvente proveniente del archivo Assembler, estos son linkeados a la hora de compilar el archivo C.

```C
#include <stdio.h>
#include <stdlib.h>
extern void formulaResolvente(float A, float B, float C);

int main(){
    float A,B,C;

    printf("Formula resolvente automatica\n\n");

    printf("Ingrese A (X^2):  ");
    scanf("%f", &A );

    printf("Ingrese B (X):  ");
    scanf("%f", &B );

    printf("Ingrese C (Termino independiente):  ");
    scanf("%f", &C );

    printf("Formula ingresada: (%.2f X^2)+(%.2f X)+(%.2f)=0\n", A, B, C);
    
    formulaResolvente(A,B,C);

    return 0;
}
```
